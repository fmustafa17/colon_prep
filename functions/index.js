const functions = require('firebase-functions');

/**********************************************************************************/
/********************************Google Assistant**********************************/
/************************************************+++++++++++++++++++++++++++++++++*/

const {
  dialogflow,
  Suggestions,
  BasicCard,
  Carousel,
  Image,
} = require('actions-on-google');


const app = dialogflow({debug: true});

// Handle the Dialogflow intent named 'Default Welcome Intent'.
app.intent('Default Welcome Intent', (conv) => {
  conv.ask('Hello! I am Kaiser Permanente\'s Colonoscopy Prep helper. You can ask me questions about prepping for colonoscopy. How can I help you?');
});


const sample_breakfastUrl = 'https://previews.123rf.com/images/akulamatiau/akulamatiau1409/akulamatiau140900129/31259496-bread-toast-with-fried-egg-and-strawberry-jam-on-white-plate.jpg'; 
const sample_lunchUrl = 'https://www.thekitchensnob.com/wp-content/uploads/2015/05/turkey-sandwich-3.jpg';
const sample_dinnerUrl = 'https://juliasalbum.com/wp-content/uploads/2016/07/28255424381_ce1de5b46c_c.jpg';
const anotherSample_breakfastUrl = 'https://assets.marthastewart.com/styles/wmax-520-highdpi/d28/med104695_0609_mellon_yogurt/med104695_0609_mellon_yogurt_sq.jpg?itok=DT40_NnR';
const anotherSample_lunchUrl = 'https://boudinbakery.com/wp-content/uploads/2017/04/sandwich-tuna-full.jpeg';
const anotherSample_dinnerUrl = 'https://bakerbynature.com/wp-content/uploads/2016/01/IMG_4303-5-4-1.jpg';


app.intent('ColonoscopyValidateLiquids', (conv, {laxative_type, liquids}) => {
  const lax = laxative_type;  
  const liq = liquids;

  if (lax === "laxative" || lax === "miralax" || lax === "gavilyte") {
    conv.ask("You should continue to drink a clear liquid diet 8 ounces every hour until your bedtime.  Do not drink any fluids within 3 hours of your procedure.");
  }
  else {
    //yes
    if (liq === "water") {
      conv.ask("yes");
    }
    else if (liq === "apple juice") {
      conv.ask("yes, and avoid juices with any pulp");
    }
    else if (liq === "tea") {
      conv.ask("yes light colored teas are okay, but do not add any milk, cream or soymilk");
    }
    else if (liq === "soda" || liq === "sports drink") {
      conv.ask("yes but please avoid liquids that are red, orange or purple");
    }
    else if (liq === "popsicle") {
      conv.ask("yes but none with any ice cream or fudge, and please avoid those red, orange or purple in color");
    }
    //no
    else if (liq === "milk" || liq === "pepsi" || liq === "tomato soup") {
      conv.ask("no");
    }
    else if(liq === "alcohol") {
      conv.ask("We do not advise to drink alcohol as a clear liquid in preparing for your colonoscopy. Alcohol can make you dehydrated while preparing for your test.");
    } 
    else if (liq === "coffee") {
      conv.ask("We do not advise to drink coffee as a clear liquid diet as it can cause dehydration.");
    }
    else if(liq === "juice") {
      conv.ask("No but apple juice is fine.");
    }
    else {
      conv.ask("Something went wrong or drink is not added." + " The parameter was " + liquids);
    }
  } //end laxative_type else statement
}); 

app.intent('colonoscopy.laxativeMisc', (conv, {fridge, finish, laxative_type, duration, mix}) => {
  const finishParam = finish;
  const fridgeParam = fridge;
  const durationParam = duration;
  const lax = laxative_type;
  const mixParam = mix;
  
  if (finishParam === "finish") {
    conv.ask("Your bowel preparation is split into two doses.  The first dose is taken at 6 pm the night before the procedure and the second dose is taken on the morning of the procedure.    On the morning of your procedure, you should wake at least 5 hours before your procedure to finish the remaining half of your bowel preparation.  It should take about 2 hours to finish this dose and you may need to drink at a faster pace to finish the laxative within this time, so plan accordingly. You must remember to stop drinking 3 hours before your appointment time, but you may take your allowed medications on the day of the procedure with a small sip of water.");
  }
  if (mixParam === "mix") {
    conv.ask("Commonly, miralax is mixed with clear liquid such as Gatorade or other electrolyte waters but avoid liquids that are colored red, orange and purple. colyte is mixed with water and flavor packets can be added for taste");
  }
  if (fridgeParam === "fridge") {
    conv.ask("If it is unopened, it does not need to be refrigerated but some patients prefer drinking the bowel preparation when it is chilled.  Once it is opened it should be refrigerated and it is good for use for up to 48 hours");         
  }
  if (durationParam === "expire") {
    conv.ask("If the bottle is unopened it is good for a year, but if the bottle is opened it should be consumed within 24-48 hours.  It does not need to be refrigerated if it is unopened.");
  }
  else if (duration === "start") {
    conv.ask("Most people will start having a bowel movement in 1-2 hours after drinking the laxative, but for some people it can take longer");
  }
  else {
    conv.ask("Something went wrong." + " The parameters were " + fridge, finish, laxative_type, duration, mix);
  }
});

app.intent('colonoscopy.bloodThinnerVisual', (conv) => {
  conv.ask("Please select from the following blood thinners");
  if (!conv.screen) {
    conv.ask("Aspirin, plavix, coumadin, pradaxa, xarelto, eliquis, savaysa, Dipyridamole, Aggrenox or Lovenox ");
  }
  else {
    conv.ask(new Suggestions('Aspirin', 'Plavix', 'Coumadin', 'Pradaxa', 'Xarelto', 'Eliquis', 'Savaysa', 'Dipyridamole', 'Aggrenox', 'Lovenox'));
  }
});

app.intent('BloodThinnerVisual - custom', (conv, {bloodThinner}) => {
  const bloodThinnerParam = bloodThinner;
  if (bloodThinnerParam === "aspirin") {
    conv.ask("You should continue aspirin up until the day of the procedure. You do not need to stop aspirin.");
  }
  else if(bloodThinnerParam === "plavix") {
    conv.ask("In general, you should stop Plavix for at least 5 days before the procedure, but you should talk to your primary care provider to make sure it is safe to do so.");
  }
  else if(bloodThinnerParam === "coumadin") {
    conv.ask("You should contact the coumadin clinic to help assist with managing this medication before the procedure.");
  }
  else if(bloodThinnerParam === "pradaxa") {
    conv.ask("The anticoagulation pharmacy will be reaching out to you to coordinate stopping this medication before the procedure.  Please call the anticoagulation pharmacy if you have more questions.");
  }
  else if(bloodThinnerParam === "dipyridamole") {
    conv.ask("You should stop dipyridamole 3 days before your procedure, but you should talk to your primary care provider if it is safe to do so.");
  }
  else if(bloodThinnerParam === "aggrenox") {
    conv.ask("You should stop aggregnox 7 days before your procedure, but you should talk to your primary care provider if it is safe to do so.");
  }
  else if(bloodThinnerParam === "lovenox") {
    conv.ask("You should stop lovenox at least 24 hours before the procedure, but you should talk to your primary care provider to make sure it is safe to do so.");
  }
  else {
    conv.ask("Something went wrong." + " The parameter is " + bloodThinnerParam);
  }
});

app.intent('colonoscopy.lowFiberDiet', (conv) => {
  conv.ask("A low residue diet is a diet that decreases the amount of stool you make and it will help the bowel preparation work better. A low fiber diet will try to limit your fiber intake to under 15 grams daily." +
  "If you would like a sample of a low fiber diet say, “give me a sample low fiber diet.” " +
  "Otherwise, if you would like to hear more details about a low fiber diet, then say “more details.” ");
});

//Follow Up Intent to colonoscopy.lowFiberDiet
app.intent('LowFiberDiet - MoreDetails', (conv) => {
  conv.ask("The five general principles of a low residue diet consist of following steps:" +
  "1. You should avoid whole-grain products, such as wheat bread or what pasta or brown rice, and products with added brain.   You can eat refined grains such as, white bread or white rice." +
  "2. You should remove the skin from vegetables and fruits.  You can eat well cooked or canned vegetables but try to avoid leafy vegetables, such as broccoli and salads.  You should also avoid beans and legumes. " +
  "3. You should avoid any foods that has seeds, nuts, popcorn and raw or dried fruit or juices with pulps or seeds" +
  "4. You should limit milk, and milk containing products such as cheese and creams, yogurts to 2 cups daily." +
  "5. You should avoid fatty and friend foods as well as tough meats, but you can eat well cooked tender meat, or fish, chicken or eggs" +
  "If you would like a sample of a low fiber diet say, “give me a sample low fiber diet.”");
});

//Follow Up Intent to colonoscopy.lowFiberDiet
app.intent('LowFiberDiet - Sample', (conv) => {
  conv.ask("Here is a sample low fiber and low residue diet. You can say “give me another sample low fiber diet” ");
  if (!conv.screen) {
    conv.ask(
      "For Breakfast, you can have eggs with toasted white bread and jam and peeled apples with no skin. " +
      "For lunch, a turkey sandwich on white bread with avocado and baked potato chips " +
      "and then you can enjoy some grilled chicken, sautéed mushrooms and white rice cooked in mushroom broth for dinner.");
  }
  else {
    return conv.ask(sampleCarousel());
  }
});

const sampleCarousel = () => {
  const carousel = new Carousel({
    items: {
      'Breakfast': {
        title: 'Breakfast',
        description: 'Eggs, toasted white bread, jam, peeled apples, no skins',
        image: new Image({
          url: sample_breakfastUrl,
          alt: 'eggs, toasted white bread, jam, peeled apples, no skins',
        }),
      },
      'Lunch': {
        title: 'Lunch',
        description: 'turkey sandwich on white bread with avocado, baked potato chips ',
        image: new Image({
          url: sample_lunchUrl,
          alt: 'turkey sandwich on white bread with avocado, baked potato chips',
        }),
      },
      'Dinner': {
        title: 'Dinner',
        description: 'Grilled chicken, sautéed mushrooms, white rice cooked in mushroom broth',
        image: new Image({
          url: sample_dinnerUrl,
          alt: 'Dinner: grilled chicken, sautéed mushrooms, white rice cooked in mushroom broth',
        }),
      },
    }
  });
  return carousel;
};

//Follow Up Intent to LowFiberDiet - Sample
app.intent('LowFiberDiet - Sample - AnotherSample', (conv) => {
  conv.ask("Here is another sample of a low fiber and low residue diet. ");
  if (!conv.screen) {
    conv.ask(
      "For Breakfast, a cantaloupe with yogurt and honey. " +
			"For lunch, tuna fish sandwich on sour dough bread and peeled pears. " +
		  "And for dinner, salmon with garlic and white rice. ");
  }
  else {
    return conv.ask(anotherSampleCarousel());
  }

});

const anotherSampleCarousel = () => {
  const carousel = new Carousel({
    items: {
      'Breakfast': {
        title: 'Breakfast',
        description: 'Cantaloupe with yogurt and honey',
        image: new Image({
          url: anotherSample_breakfastUrl,
          alt: 'Cantaloupe with yogurt and honey',
        }),
      },
      'Lunch': {
        title: 'Lunch',
        description: 'Tuna fish sandwich on sour dough bread, peeled pears',
        image: new Image({
          url: anotherSample_lunchUrl,
          alt: 'Tuna fish sandwich on sour dough bread, peeled pears',
        }),
      },
      'Dinner': {
        title: 'Dinner',
        description: 'Salmon with garlic, white rice',
        image: new Image({
          url: anotherSample_dinnerUrl,
          alt: 'Salmon with garlic, white rice',
        }),
      },
    }
  });
  return carousel;
};

// Set the DialogflowApp object to handle the HTTPS POST request.
exports.dialogflowFirebaseFulfillment = functions.https.onRequest(app);


/**********************************************************************************/
/********************************Alexa Skill***************************************/
/************************************************+++++++++++++++++++++++++++++++++*/
// const Alexa = require('ask-sdk-core');

exports.alexaSkill = functions.https.onRequest((request, response) => {
  const type = JSON.stringify(request.body.request.type);
  const name = JSON.stringify(request.body.request.intent.name);
  var liquidsSlot = JSON.stringify(request.body.request.intent.slots.liquidsslot.resolutions.resolutionsPerAuthority[0].values[0].value.name);
  /*
  var finishSlot = JSON.stringify(request.body.request.intent.slots.finishslot.resolutions.resolutionsPerAuthority[0].values[0].value.name);
  var fridgeSlot = JSON.stringify(request.body.request.intent.slots.fridgeslot.resolutions.resolutionsPerAuthority[0].values[0].value.name);
  var laxative_typeSlot = JSON.stringify(request.body.request.intent.slots.laxative_typeslot.resolutions.resolutionsPerAuthority[0].values[0].value.name);
  var mixSlot = JSON.stringify(request.body.request.intent.slots.mixslot.resolutions.resolutionsPerAuthority[0].values[0].value.name);
  */
  const result = getAlexaResponse(type, name, liquidsSlot, mixSlot, laxative_typeSlot, finishSlot, fridgeSlot);

  response.send(result);
});

const getAlexaResponse = (type, name, liquidsSlot, mixSlot, laxative_typeSlot, finishSlot, fridgeSlot) => {
  var AlexaDefaultAnswer = {
    "version": "1.0",
    "response": {
      "outputSpeech": {
        "type": "SSML",
        "ssml": "<speak>Hello! I am Kaiser Permanente's Colonoscopy Prep helper. You can ask me questions about prepping for colonoscopy. How can I help you?</speak>"
      },
      "shouldEndSession": false,
      "card": {
        "type": "Simple",
        "title": "LaunchRequest",
        "content": "Welcome to the Alexa Skills Kit, you can say hello!"
      }
    },
    "userAgent": "ask-node/2.3.0 Node/v8.10.0",
    "sessionAttributes": {}
  }

  if(type === '"LaunchRequest"') {
      return AlexaDefaultAnswer;
  } else if(type === '"IntentRequest"' && name === '"ColonoscopyValidateLiquids"'){
      AlexaDefaultAnswer.response.outputSpeech.ssml = "<speak>" + alexaValidateLiquids(liquidsSlot) + "</speak>";
      AlexaDefaultAnswer.response.card.content = alexaValidateLiquids(liquidsSlot);
      return AlexaDefaultAnswer;
  }
  else if (type === '"IntentRequest"' && name === '"ColonoscopyLaxativemisc"') {
    AlexaDefaultAnswer.response.outputSpeech.ssml = "<speak>" + alexaLaxativeMisc(mixSlot, laxative_typeSlot, finishSlot, fridgeSlot) + "</speak>";
    AlexaDefaultAnswer.response.card.content = alexaLaxativeMisc(mixSlot, laxative_typeSlot, finishSlot, fridgeSlot);
    return AlexaDefaultAnswer;
  }
  else {
    return AlexaDefaultAnswer;
  }
};

/**********************************************************************************/
/***********************************ANSWERS****************************************/
/************************************************+++++++++++++++++++++++++++++++++*/

function alexaValidateLiquids(liquidsSlot) {
  var response;
  const liq = liquidsSlot;
  // const lax = agent.parameters.laxative_type;
  // if (lax === '"laxative"' || lax === '"miralax"' || lax === '"gavilyte"') {
  //   response = "You should continue to drink a clear liquid diet 8 ounces every hour until your bedtime.  Do not drink any fluids within 3 hours of your procedure.";
  // }
  // else { 
  
  //yes
  if (liq === '"water"') {
    response = "yes";
  }
  else if (liq === '"apple juice"') {
    response = "yes, and avoid juices with any pulp";
  }
  else if (liq === '"tea"') {
    response = "yes light colored teas are okay, but do not add any milk, cream or soymilk";
  }
  else if (liq === '"soda"' || liq === '"sports drink"') {
    response = "yes but please avoid liquids that are red, orange or purple";
  }
  else if (liq === '"popsicle"') {
    response = "yes but none with any ice cream or fudge, and please avoid those red, orange or purple in color";
  }
  //no
  else if (liq === '"milk"' || liq === '"pepsi"' || liq === '"tomato soup"') {
    response = "no";
  }
  else if(liq === '"alcohol"') {
    response = "We do not advise to drink alcohol as a clear liquid in preparing for your colonoscopy. Alcohol can make you dehydrated while preparing for your test.";
  } 
  else if (liq === '"coffee"') {
    response = "We do not advise to drink coffee as a clear liquid diet as it can cause dehydration.";
  }
  else if(liq === '"juice"') {
    response = "No but apple juice is fine.";
  }
  else {
    response = "Something went wrong or drink is not added." + " The slot is " + liquidsSlot;
  }
  // } //end laxative_type else
  return response;
}

function alexaLaxativeMisc(mixSlot, laxative_typeSlot, finishSlot, fridgeSlot) {
  var response;
  var mix = mixSlot;
  var lax = laxative_typeSlot;
  var finish = finishSlot;
  var fridge = fridgeSlot;
  
  if (mix === '"mix"') {
    response = "Commonly, miralax is mixed with clear liquid such as Gatorade or other electrolyte waters but avoid liquids that are colored red, orange and purple. colyte is mixed with water and flavor packets can be added for taste";
  }
  else if (fridge === '"fridge"') {
    response = "If it is unopened, it does not need to be refrigerated but some patients prefer drinking the bowel preparation when it is chilled.  Once it is opened it should be refrigerated and it is good for use for up to 48 hours";
  }
  else if (finish === '"finish"') {
    response = "Your bowel preparation is split into two doses.  The first dose is taken at 6 pm the night before the procedure and the second dose is taken on the morning of the procedure.    On the morning of your procedure, you should wake at least 5 hours before your procedure to finish the remaining half of your bowel preparation.  It should take about 2 hours to finish this dose and you may need to drink at a faster pace to finish the laxative within this time, so plan accordingly. You must remember to stop drinking 3 hours before your appointment time, but you may take your allowed medications on the day of the procedure with a small sip of water.";
  }
  else if (finish === '"expire"') {
    response = "If the bottle is unopened it is good for a year, but if the bottle is opened it should be consumed within 24-48 hours.  It does not need to be refrigerated if it is unopened.";
  }
  else {
    response = "Something went wrong." + " The slots are " + mixSlot, laxative_typeSlot, finishSlot, fridgeSlot ;
  }
  //else if (finish === "last") {
  //		agent.add("If the bottle is unopened it is good for a year, but if the bottle is opened it should be consumed within 24-48 hours.  It does not need to be refrigerated if it is unopened.");
  //}

  // if (duration === '"expire"') {
  //   response = "If the bottle is unopened it is good for a year, but if the bottle is opened it should be consumed within 24-48 hours.  It does not need to be refrigerated if it is unopened.";
  // }
  // if (duration === '"start"') {
  //   response = "most people will start having a bowel movement in 1-2 hours after drinking the laxative, but for some people it can take longer";
  // }
  return response;
}